var mydiv=document.getElementById('End');
var endTimeUnix = mydiv.getAttribute("data-end");

var timer = setInterval(function() {
    var nowFormated = new Date()
    var now = nowFormated.getTime();
    var end = endTimeUnix*1000;
    var remainingTime = Math.ceil((end - now)/1000);

    if (remainingTime >= 0) {
        document.getElementById("timer-remaining").innerHTML = (remainingTime)
    } else {
        document.getElementById("timer-remaining").innerHTML = "BOUM!";
    }
    document.getElementById("timer-current").innerHTML = (nowFormated.toString())
}, 1000);