package main

import (
	"html/template"
	"log"
	"math/rand"
	"net/http"
	"time"
)

//CountdownMax is the maximum countdown tims
var CountdownMax = 300

// PageVariables represents Date and Time for homepage
type PageVariables struct {
	TimeStart string
	TimeEnd   string
	End       int64
}

func main() {
	http.HandleFunc("/", HomePage)

	fs := http.FileServer(http.Dir("static"))
	http.Handle("/static/", http.StripPrefix("/static/", fs))

	log.Println("Listening...")

	log.Fatal(http.ListenAndServe(":8080", nil))
}

//HomePage renders the homepage
func HomePage(w http.ResponseWriter, r *http.Request) {

	start := time.Now()
	var countdown = rand.Intn(CountdownMax + 1) //random number between 0 and COUNTDOWN_MAX included
	end := start.Add(time.Duration(countdown) * time.Second)

	HomePageVars := PageVariables{
		TimeStart: start.Format("02-01-2006    15:04:05"), //This is how timeformat is chosen in Go (is a string)
		TimeEnd:   end.Format("02-01-2006    15:04:05"),
		End:       end.Unix(),
	}

	t, err := template.ParseFiles("views/homepage.html") //parse the html file homepage.html
	if err != nil {                                      //this is the only way to handle errors in go, if i'm not mistaken

		log.Print("template parsing error: ", err)
	} else {
		err = t.Execute(w, HomePageVars) //execute the template and pass it the HomePageVars struct to fill in the gaps
		if err != nil {
			log.Print("template executing error: ", err)
		}
	}
}
